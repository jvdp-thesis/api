# FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

# COPY requirements.txt /app/requirements.txt

# RUN pip install -r /app/requirements.txt

# # Manually install tweepy, as the alpha version has some required bugfixes.

# # RUN git clone https://github.com/tweepy/tweepy.git
# # RUN pip install tweepy/

# COPY download_dependencies.py /download_dependencies.py

# RUN python /download_dependencies.py

# COPY ./app /app
# COPY alembic.ini /alembic.ini

FROM python:3.8

EXPOSE 80

COPY requirements.txt /app/requirements.txt
COPY download_dependencies.py /download_dependencies.py
COPY ./app /app
COPY alembic.ini /alembic.ini

RUN git clone --single-branch --branch v3.10.0-modified https://github.com/JaccovdP/tweepy.git
RUN pip install tweepy/

RUN pip install -r /app/requirements.txt
RUN python /download_dependencies.py

WORKDIR /app

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]