# Run locally
```bash
git clone https://gitlab.com/jvdp-thesis/api.git
cd thesis-api
pip install -r requirements.txt
cd app
uvicorn main:app --reload
```

# API docs
After starting the local server, the API docs are available at http://localhost:8000/docs

# Deployment

## Docker compose
```yaml
version: '3.1'

services:
  
  db:
    image: postgres
    container_name: smid-db
    restart: always
    environment:
      POSTGRES_PASSWORD: password
    ports:
        - 5432:5432
    volumes:
        - path_to_db_on_host:/var/lib/postgresql/data
  api:
    image: registry.gitlab.com/jvdp-thesis/api:latest
    container_name: smid-api
    restart: always
    environment:
      THESIS_ENV: DEV
      DB_HOST: db_host
      DB_PORT: 5432
      DB_PASS: password
      TW_CONSUMER_KEY: 1234
      TW_CONSUMER_SECRET: 1234
      TW_ACCESS_TOKEN: 1234
      TW_ACCESS_TOKEN_SECRET: 1234
    ports:
        - 8000:80
  frontend:
    image: registry.gitlab.com/jvdp-thesis/frontend:latest
    container_name: smid-frontend
    restart: always
    ports:
        - 8080:80
```

## Reverse proxy
When the THESIS_ENV environment variable is set, the API root path is expected to be /api. Use the following NGINX configuration to setup the reverse proxy. Replace the host with the IP of your server.

```nginx
location /api/ {
        proxy_pass http://host:port/; 
}

location ^~ /api/ws {
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_pass http://host:port/ws; 
}

location ^~ /api/ws_tweets {
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_pass http://host:port/ws_tweets; 
}

```
