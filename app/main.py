from asyncio.tasks import wait
import os
from fastapi import Depends, FastAPI, HTTPException, WebSocket, WebSocketDisconnect, Query
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from typing import List, Optional
import asyncio

import ws_manager, schemas, models, crud, functions
from database import SessionLocal, engine

env = os.environ.get("THESIS_ENV", "LOCAL")
if(env != "LOCAL"):
    api_path = "/api"
else:    
    api_path = ""

functions.log.info("SMID - Environment: " + env)
functions.log.info("SMID - API path: " + api_path)


if not os.path.exists("data/media"):
    os.makedirs("data/media")

author = "Jacco van der Plaat"

tags_metadata = [
    {
        "name": "Downloads",
        "description": "API calls to download available data in CSV format"
    },
    {
        "name": "Add data",
        "description": "API calls to add new data"
    },
    {
        "name": "Request data",
        "description": "API calls that return data in JSON format"
    }
]

app = FastAPI(
    title="SMID API",
    description="Thesis API - Jacco van der Plaat",
    version="1.0.0",
    root_path=api_path
)

app.mount("/media", StaticFiles(directory="data/media"), name="media")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close() # pylint: disable=no-member

models.Base.metadata.create_all(bind=engine)

ws = None

# GET REQUESTS
@app.get("/")
async def root():
    return {
        "title": app.title, 
        "description": app.description,
        "author": author,
        "version": app.version, 
        "root_path": app.root_path, 
        "env": env}

@app.on_event("startup")
async def startup_event(db = SessionLocal()):
    global ws
    ws = ws_manager.get_manager()
    functions.load_list_of_places()
    if(env != "LOCAL" and env != "TEST"):
        await functions.get_tweets_stream(['demonstratie'], db)

@app.get("/replay/", tags=["Replay data"])
async def replay_data(set: Optional[str] = None, modifier: Optional[int] = 1, db: Session = Depends(get_db)):
    await functions.replay_tweets(set, modifier, db)

@app.get("/replay_instantly/", tags=["Replay data"])
async def replay_data_instantly(set: Optional[str] = None, db: Session = Depends(get_db)):
    await functions.replay_tweets_instant(set, db)

@app.get("/get_original_tweets/{set}", tags=["Admin functions"])
async def get_original_tweets(set: str, db: Session = Depends(get_db)):
    await functions.get_original_tweets_for_dataset(set, db)

@app.get("/incidents/", tags=["Request data"])
async def get_incidents(status: Optional[str] = None, db: Session = Depends(get_db)):
    incidents = crud.get_incidents(db, status=status)
    if incidents is None:
        raise HTTPException(status_code=404, detail="Incidents not found")
    return incidents

@app.get("/incidents_tweets/", tags=["Request data"])
async def get_incidents_with_tweets(status: Optional[str] = None, db: Session = Depends(get_db)):
    incidents = crud.get_incidents_with_tweets(db, status=status)
    if incidents is None:
        raise HTTPException(status_code=404, detail="Incidents not found")
    return incidents

@app.get("/incident/{incident_id}", tags=["Request data"])
async def get_incident(incident_id: int, db: Session = Depends(get_db)):
    incident = crud.get_formatted_incident(db, incident_id=incident_id)
    if incident is None:
        raise HTTPException(status_code=404, detail="Incident not found")
    return incident

@app.get("/tweet/{tweet_id}", tags=["Request data"])
async def get_tweet(tweet_id: int, db: Session = Depends(get_db)):
    tweet = crud.get_tweet(db, status_id=tweet_id)
    if tweet is None:
        raise HTTPException(status_code=404, detail="Tweet not found")
    return tweet

@app.get("/tweets/", tags=["Request data"])
async def get_tweets(tweet_ids: List[int] = Query(None), db: Session = Depends(get_db)):
    tweets = crud.get_tweets(db, status_ids=tweet_ids)
    if tweets is None:
        raise HTTPException(status_code=404, detail="Tweets not found")
    return tweets

# DOWNLOADS
@app.get("/download/incidents", tags=["Downloads"])
async def download_incidents(db: Session = Depends(get_db)):
    incidents = crud.get_all_incidents(db)
    return functions.create_csv_response(incidents, "incidents.csv", columns=['name', 'description', 'location', 'related_tweets', 'status', 'created', 'updated'])

@app.get("/download/tweets", tags=["Downloads"])
async def download_tweets(db: Session = Depends(get_db)):
    tweets = crud.get_all_tweets(db)
    return functions.create_csv_response(tweets, "tweets.csv")

@app.get("/coordinates/{place}")
async def coordinates(place):
    return functions.coordinates_for_place(place)

# END GET REQUESTS

# POST REQUESTS
@app.post("/tweets_media/", tags=["Request data"])
async def get_tweet_media(tweet_ids: List[str]):
    response = []
    for tweet_id in tweet_ids:
        dir = "data/media/" + tweet_id
        if(os.path.exists(dir)):
            for image in os.listdir(dir):
                if(os.path.isfile(os.path.join(dir, image))):
                    response.append({"id": tweet_id, "src": image})
    return response

@app.post("/incident", 
    tags=["Add data"],
    description="Add a new incident."
)
async def create_incident(incident: schemas.IncidentCreate, db: Session = Depends(get_db)):
    incident = jsonable_encoder(crud.create_incident(db, incident=incident))
    if incident is None:
        message = {"message_type": "error", "error_msg": "db_error"}
        await ws.broadcast(message)
        raise HTTPException(status_code=500, detail="Internal server error")
    
    loop = asyncio.get_event_loop()
    loop.create_task(functions.extract_media(incident["related_tweet_ids"], db))
    message = incident
    message["message_type"] = "new_incident"
    await ws.broadcast(message)
    functions.log.info("SMID: Incident created with ID " + str(incident["id"]))
    return incident["id"]

@app.post("/link_tweet", 
    response_model=schemas.IncidentTweetRelation, 
    tags=["Add data"],
    description="Link an existing Tweet to an existing incident."
)
async def link_tweet(relation: schemas.IncidentTweetRelationCreate, db: Session = Depends(get_db)):
    relation = jsonable_encoder(crud.create_relation(db, relation=relation))
    if relation is None:
        message = {"message_type": "error", "error": "db_error"}
        await ws.broadcast(message)
        raise HTTPException(status_code=500, detail="Internal server error")

    loop = asyncio.get_event_loop()
    loop.create_task(functions.extract_media([ relation["tweet_id"] ], db))
    message = relation
    message["message_type"] = "new_relation"
    tweet = jsonable_encoder(crud.get_tweet(db, message["tweet_id"]))
    if tweet is not None:
        message["tweet"] = jsonable_encoder(crud.get_tweet(db, message["tweet_id"]))
    await ws.broadcast(message)
    functions.log.info("SMID: Tweet with ID " + str(relation["tweet_id"]) + " linked to incident with ID " + str(relation["incident_id"]))
    return relation

# END POST REQUESTS

# SOCKETS
async def send_websocket_update(ws, db):
    incidents = jsonable_encoder(crud.get_incidents_with_tweets(db, status='open'))
    incidents = {"message_type": "open_incidents", "data": incidents }
    await ws.broadcast(incidents)
    await ws.broadcast({"message_type": "nr_detection_systems", "nr": len(ws.active_tweet_connections)})

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, db: Session = Depends(get_db)):
    await ws.connect(websocket)
    try:
        while True:
            data = await websocket.receive_json()
            if data["message"] == 'getOpenIncidents':
                await send_websocket_update(ws, db)
            elif data["message"] == 'delete':
                crud.delete_incident(db, int(data["id"]))
                await send_websocket_update(ws, db)
            elif data["message"] == 'confirm':
                crud.confirm_incident(db, int(data["id"]))
                await send_websocket_update(ws, db)
            elif data["message"] == 'complete':
                crud.complete_incident(db, int(data["id"]))
                await send_websocket_update(ws, db)
    except WebSocketDisconnect:
        ws.disconnect(websocket)

@app.websocket("/ws_tweets")
async def websocket_tweet_endpoint(websocket: WebSocket):
    await ws.connect_tweet_endpoint(websocket)
    try:
        while True:
            data = await websocket.receive_json()
            print(data)
    except WebSocketDisconnect:
        await ws.disconnect_tweet_endpoint(websocket)

@app.websocket("/ws_tweets_replay")
async def websocket_replay_endpoint(websocket: WebSocket):
    await ws.connect_replay_endpoint(websocket)
    try:
        while True:
            data = await websocket.receive_json()
            print(data)
    except WebSocketDisconnect:
        await ws.disconnect_replay_endpoint(websocket)