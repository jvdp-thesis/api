# Pre-processing code by Joren Wouters, adjusted for use within this API

import re
import emoji
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import functions

# Code by Joren Wouters, adjusted for use within this API
# Use Dutch stop words
stop_words = stopwords.words('dutch') + ["rt", "nan", "NaN"] 

# Function to get only the place if tweet.place is acquired as a whole
def clean_place(place):
    if place != None:
        place = place.name
        
        return place
    
# Function to clean the coordinates columns
def clean_coordinates(coordinate):
    import ast
    
    # Check if the coordinates has any value
    if type(coordinate) == str:
        coordinate = ast.literal_eval(coordinate)  # Change the string to a dictionary (so we can get the necessary elements)
        i = 0
        for element in coordinate:
            if(i==0) :                             # This will skip the first element (not necessary)
                i = i+1
            else:
                return coordinate['coordinates']   # Return only the coordinates
    else:                                          # If the row is not a string it always is a nan, so we can set this to None
        return None
    
# Clean the text of the tweet
def clean_text(tweet, hashtag_text='keep', representation = 'string'):
    
    # Parameters
    # hashtag_text, default = 'keep'
        # 'keep' - keeps the hashtag text and only removes the '#' in the text
        # 'lose' - both removes the hashtag text and the '#' in the text
    # representation, default = 'string'
        # 'list' - returns a list of words
        # 'string' - returns a sentence in string format
    
    # Make the tweet lowercase
    tweet = tweet.lower()
    
    # Remove words with less than two characters
    tweet = re.sub(r'\b\w{1,2}\b', '', tweet)
    
    # Remove URLs
    tweet = remove_url(tweet)
    
    # Remove punctuations unless they are part of a digit (such as "5.000")
    tweet = re.sub(r'(?:(?<!\d)[.,;:…‘]|[.,;:…‘](?!\d))', '', tweet)
    
    # Remove emojis
    tweet = emoji.get_emoji_regexp().sub(u'', tweet)
    
    if hashtag_text == 'keep':
        tweet = tweet.replace("#", "")
        tweet = ' '.join(re.sub("(@[A-Za-z0-9]+)", "", tweet).split())
    else:
        # Remove hashtags and mentions
        tweet = ' '.join(re.sub("(@[A-Za-z0-9]+)|(#[A-Za-z0-9]+)", "", tweet).split())
    
    # Remove non-alphanumeric charachters, line breaks and tabs
    tweet = ' '.join(re.sub("([:/\!@#$%^&*()_+{}[\];\"\'|?<>~`\-\n\t’])", "", tweet).split())
    
    # Tokenize the tweet
    tweet = word_tokenize(tweet)
    
    # Remove stopwords
    tweet = [w for w in tweet if not w in stop_words]
    
    if representation == 'list':
        return tweet
    else:
        return listToString(tweet)

# Function to convert a list to a string
def listToString(s):  
    
    # initialize an empty string 
    str1 = " " 
    
    # return string   
    return (str1.join(s)) 

def remove_url(tweet_text):
    if has_url_regex(tweet_text): 
        url_regex_list = regex_url_extractor(tweet_text)
        for url in url_regex_list:
            tweet_text = tweet_text.replace(url, "")
    return tweet_text

def has_url_regex(tweet_text):
    return regex_url_extractor(tweet_text)

def regex_url_extractor(tweet_text):
    return re.findall('https?:\/\/(?:[-\w\/.]|(?:%[\da-fA-F]{2}))+', tweet_text)
    
def clean_field(field, type_field):
    if (field != "[]") and (field != None):                              # Check if the field has any value
        field_list = []
        for element in field:
            if (type_field == 'hashtag'):                   # Check if the field is hashtag field
                field_list.append(element["text"])
            elif (type_field == 'user_mentions'):        # Check if the field is a user_mentions field
                field_list.append(element["screen_name"])
            elif (type_field == 'media'):                # Check if the field is a media field
                field_list.append(element["media_url"])
            elif (type_field == 'urls'):
                field_list.append(element["expanded_url"])
        return field_list
    else:
        return None

def cleanTweet(status):
       
    # Get the hashtags and clean them
    hashtags = status.get("entities").get("hashtags", None)
    hashtags = clean_field(hashtags, type_field='hashtags')
    
    # Get the coordinates and clean them
    coordinates = status.get("coordinates")
    coordinates = clean_coordinates(coordinates)
    
    # Get the user determined place and clean it
    place = status.get("place")
    place = clean_place(place)
    
    # Get the text of the tweet and clean it
    text = status.get("text")
    preprocessed_text = clean_text(text, 'keep', 'string')
    preprocessed_text_no_hashtag = clean_text(text, 'lose', 'string')
    preprocessed_text_tokenized = clean_text(text, 'keep', 'list')
    preprocessed_text_tokenized_no_hashtag = clean_text(text, 'lose', 'list')
    
    # Get the user mentions and clean it
    user_mentions = status.get("entities").get("user_mentions")
    user_mentions = clean_field(user_mentions, type_field='user_mentions')
    
    # If a tweet has an image, get the URL to the image
    if hasattr(status, 'extended_entities'):
        media = status.get("extended_entities").get("media")
        media = clean_field(media, type_field='media')
    else:
        media = None
    
    # Get URLs mentioned in a tweet
    urls = status.get("entities").get("urls")
    urls = clean_field(urls, type_field="urls")

    if(place != None):
        osm_coordinates = functions.coordinates_for_place(str(place))
    else:
        osm_coordinates = None

    places_in_text = list(set(preprocessed_text_tokenized).intersection(functions.list_of_places))

    pre_places_in_text = []

    for i in range(len(places_in_text)):
        pre_places_in_text.append({ "place": places_in_text[i], "coordinates": functions.list_of_places_coordinates[places_in_text[i]] })

    cleaned_dict_object = {
        "pre_hashtags" : str(hashtags), 
        "pre_coordinates": str(coordinates), 
        "pre_place" : str(place),
        "pre_osm_coordinates": str(osm_coordinates),
        "pre_text": str(preprocessed_text),
        "pre_text_no_hashtag": str(preprocessed_text_no_hashtag), 
        "pre_text_tokenized": str(preprocessed_text_tokenized),
        "pre_text_tokenized_no_hashtag": str(preprocessed_text_tokenized_no_hashtag), 
        "pre_media": str(media),
        "pre_urls": str(urls),
        "pre_places_in_text": str(pre_places_in_text)
    }

    for key, value in status.items():
        if key == "status_id":
            status[key] = value
        else:
            status[key] = str(value)

    return {**status, **cleaned_dict_object}

# End code by Joren Wouters