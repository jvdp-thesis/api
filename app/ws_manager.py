from fastapi import WebSocket
from typing import List
import asyncio

class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []
        self.active_tweet_connections: List[WebSocket] = []
        self.active_replay_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    async def connect_tweet_endpoint(self, websocket: WebSocket):
        await websocket.accept()
        self.active_tweet_connections.append(websocket)
        for connection in self.active_connections:
            await connection.send_json({"message_type": "nr_detection_systems", "nr": len(self.active_tweet_connections)})

    async def connect_replay_endpoint(self, websocket: WebSocket):
        await websocket.accept()
        self.active_replay_connections.append(websocket)
        for connection in self.active_connections:
            await connection.send_json({"message_type": "nr_replay_detection_systems", "nr": len(self.active_replay_connections)})

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def disconnect_tweet_endpoint(self, websocket: WebSocket):
        self.active_tweet_connections.remove(websocket)
        for connection in self.active_connections:
            await connection.send_json({"message_type": "nr_detection_systems", "nr": len(self.active_tweet_connections)})

    async def disconnect_replay_endpoint(self, websocket: WebSocket):
        self.active_replay_connections.remove(websocket)
        for connection in self.active_connections:
            await connection.send_json({"message_type": "nr_replay_detection_systems", "nr": len(self.active_replay_connections)})

    async def broadcast(self, data):
        for connection in self.active_connections:
            await connection.send_json(data)
    
    async def broadcast_tweet(self, data):
        for connection in self.active_tweet_connections:
            await connection.send_json(data)

    async def broadcast_replay_tweet(self, data):
        for connection in self.active_replay_connections:
            await connection.send_json(data)

manager = ConnectionManager()

def get_manager():
    global manager
    return manager