from sqlalchemy.orm import Session

import models, schemas

def get_incidents(db: Session, status: str):
    open_status = ["confirmed", "new"]
    closed_status = ["dismissed", "done"]
    if status == "open":
        db_incidents = db.query(models.Incident).filter(models.Incident.status.in_(open_status)).all()
    elif status == "closed":
        db_incidents = db.query(models.Incident).filter(models.Incident.status.in_(closed_status)).all()
    elif status != None:
        db_incidents = db.query(models.Incident).filter(models.Incident.status == status).all()
    else:
        db_incidents = db.query(models.Incident).all()
    incidents = []
    for db_incident in db_incidents:
        incident = db_incident.__dict__
        tweets = []
        for i in range(0, len(incident["related_tweets"])):
            tweets.append(str(incident["related_tweets"][i].tweet_id))
        incident["related_tweet_ids"] = tweets
        incidents.append(incident)
    return incidents

def get_incidents_with_tweets(db: Session, status: str):
    open_status = ["confirmed", "new"]
    closed_status = ["dismissed", "done"]
    if status == "open":
        db_incidents = db.query(models.Incident).filter(models.Incident.status.in_(open_status)).all()
    elif status == "closed":
        db_incidents = db.query(models.Incident).filter(models.Incident.status.in_(closed_status)).all()
    elif status != None:
        db_incidents = db.query(models.Incident).filter(models.Incident.status == status).all()
    else:
        db_incidents = db.query(models.Incident).all()
    incidents = []
    for db_incident in db_incidents:
        incident = db_incident.__dict__
        tweets = []
        for i in range(0, len(incident["related_tweets"])):
            tweet = str(incident["related_tweets"][i].tweet_id)
            tweets.append(tweet)
        incident["related_tweet_ids"] = tweets
        incident["related_tweets"] = get_tweets(db, tweets)
        incidents.append(incident)
    return incidents

def get_formatted_incident(db: Session, incident_id: int):
    db_incident = db.query(models.Incident).filter(models.Incident.id == incident_id).first()
    incident = db_incident.__dict__
    tweets = []
    for i in range(0, len(incident["related_tweets"])):
        tweets.append(str(incident["related_tweets"][i].tweet_id))
    incident["related_tweets"] = get_tweets(db, tweets)
    incident["related_tweet_ids"] = tweets
    return incident

def get_tweet(db: Session, status_id: int):
    return db.query(models.Tweet).filter(models.Tweet.status_id == status_id).first()

def get_tweets(db: Session, status_ids: int):
    tweets = db.query(models.Tweet).filter(models.Tweet.status_id.in_(status_ids)).all()
    for i in range(0, len(tweets)):
        tweets[i].status_id = str(tweets[i].status_id)
    return tweets

def get_all_incidents(db: Session):
    db_incidents = db.query(models.Incident).all()
    incidents = []
    for db_incident in db_incidents:
        incident = db_incident.__dict__
        tweets = []
        for i in range(0, len(incident["related_tweets"])):
            tweets.append(str(incident["related_tweets"][i].tweet_id))
        incident["related_tweet_ids"] = tweets
        incidents.append(incident)
    return incidents


def get_all_tweets(db: Session):
    result = db.query(models.Tweet).all()
    return [tweet.__dict__ for tweet in result]

def create_incident(db: Session, incident: schemas.IncidentCreate):
    
    db_incident = models.Incident(
        name = incident.name,
        description = incident.description,
        location = incident.location,
        status = incident.status
    )
    db.add(db_incident)
    db.commit()
    db.refresh(db_incident)
    incident_id = db_incident.id

    tweets = []
    for i in range(0, len(incident.related_tweets)):
        db_related_tweet = models.IncidentTweetRelation(
            incident_id = incident_id,
            tweet_id = incident.related_tweets[i]
        )
        tweets.append(db_related_tweet)
    
    db.add_all(tweets)
    db.commit()
    db.refresh(db_incident)
    response = db_incident.__dict__
    tweets = []
    for i in range(0, len(response["related_tweets"])):
        tweets.append(str(response["related_tweets"][i].tweet_id))
    response["related_tweet_ids"] = tweets
    response["related_tweets"] = get_tweets(db, tweets)
    return response

def create_tweet(db: Session, tweet: schemas.TweetCreate):
    db_tweet = models.Tweet(
        **tweet
    )
    db.merge(db_tweet)
    db.commit()
    return True

def create_relation(db: Session, relation: schemas.IncidentTweetRelationCreate):
    db_relation = models.IncidentTweetRelation(
        **relation.dict()
    )
    db.add(db_relation)
    db.commit()
    db.refresh(db_relation)
    return db_relation


def delete_incident(db: Session, id):
    db.query(models.Incident).filter(models.Incident.id == id).update({'status': 'dismissed'})
    db.commit()
    return True

def confirm_incident(db: Session, id):
    db.query(models.Incident).filter(models.Incident.id == id).update({'status': 'confirmed'})
    db.commit()
    return True

def complete_incident(db: Session, id):
    db.query(models.Incident).filter(models.Incident.id == id).update({'status': 'done'})
    db.commit()
    return True