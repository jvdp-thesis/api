from sqlalchemy import Table, Boolean, Column, ForeignKey, Integer, String, Date, DateTime, BigInteger
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from database import Base

class IncidentTweetRelation(Base):
    __tablename__ = 'incident_tweet_relation'

    created = Column(DateTime, server_default=func.now())
    updated = Column(DateTime, server_default=func.now(), server_onupdate=func.now())

    incident_id = Column(BigInteger, ForeignKey('incidents.id'), primary_key=True, nullable=False)
    tweet_id = Column(BigInteger, primary_key=True, nullable=False)

    incident = relationship("Incident", back_populates="related_tweets")

class Incident(Base):
    __tablename__ = "incidents"

    id = Column(BigInteger, primary_key=True, index=True)
    created = Column(DateTime, server_default=func.now())
    updated = Column(DateTime, server_default=func.now(), server_onupdate=func.now())
    name = Column(String)
    description = Column(String)
    location = Column(String)
    status = Column(String)

    related_tweets = relationship("IncidentTweetRelation", back_populates="incident", lazy="joined")

class Tweet(Base):
    __tablename__ = "tweets"

    created = Column(DateTime, server_default=func.now())
    updated = Column(DateTime, server_default=func.now(), server_onupdate=func.now())
    created_at = Column(String)
    status_id = Column(BigInteger, primary_key=True)
    text = Column(String)
    coordinates = Column(String)
    source = Column(String)
    in_reply_to_status_id = Column(String)
    in_reply_to_user_id = Column(String)
    in_reply_to_screen_name = Column(String)
    retweeted = Column(String)
    retweeted_status = Column(String)
    retweeted_status_id = Column(String)
    retweeted_status_user_id = Column(String)
    favorited = Column(String)
    favorite_count = Column(String)
    place = Column(String)
    entities = Column(String)
    extended_entities = Column(String)
    user = Column(String)
    user_id = Column(String)
    user_screen_name = Column(String)
    possibly_sensitive = Column(String)
    lang = Column(String)
    pre_hashtags = Column(String)
    pre_coordinates = Column(String)
    pre_place = Column(String)
    pre_osm_coordinates = Column(String)
    pre_text = Column(String)
    pre_text_no_hashtag = Column(String)
    pre_text_tokenized = Column(String)
    pre_text_tokenized_no_hashtag = Column(String)
    pre_media = Column(String)
    pre_urls = Column(String)
    pre_places_in_text = Column(String)

