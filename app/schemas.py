from pydantic import BaseModel, Field
from typing import Optional, List, Tuple
from datetime import datetime

# Incident schema
class IncidentBase(BaseModel):
    name: Optional[str] = None
    description: Optional[str] = None
    location: Optional[Tuple[float, float]]
    status: Optional[str] = "new"

class IncidentCreate(IncidentBase):
    related_tweets: List[int] = []

class Incident(IncidentBase):
    id: int
    created: datetime
    updated: datetime
    related_tweets: list

    class Config:
        orm_mode = True

# Tweet schema
class TweetBase(BaseModel):
    created_at: str
    status_id: int
    text: Optional[str]
    coordinates: Optional[str]
    source: Optional[str]
    in_reply_to_status_id: Optional[str]
    in_reply_to_user_id: Optional[str]
    in_reply_to_screen_name: Optional[str]
    retweeted: Optional[str]
    retweeted_status: Optional[str]
    retweeted_status_id: Optional[str]
    retweeted_status_user_id: Optional[str]
    favorited: Optional[str]
    favorite_count: Optional[str]
    place: Optional[str]
    entities: Optional[str]
    extended_entities: Optional[str]
    user: Optional[str]
    user_id: Optional[str]
    user_screen_name: Optional[str]
    possibly_sensitive: Optional[str]
    lang: Optional[str]
    pre_hashtags: Optional[str]
    pre_coordinates: Optional[str]
    pre_place: Optional[str]
    pre_osm_coordinates: Optional[str]
    pre_text: Optional[str]
    pre_text_no_hashtag: Optional[str]
    pre_text_tokenized: Optional[str]
    pre_text_tokenized_no_hashtag: Optional[str]
    pre_media: Optional[str]
    pre_urls: Optional[str]
    pre_places_in_text: Optional[str]

class TweetCreate(TweetBase):
    pass

class Tweet(TweetBase):
    created: datetime
    updated: datetime

    class Config:
        orm_mode = True

# Incident-Tweet Relation schema
class IncidentTweetRelationBase(BaseModel):
    incident_id: int
    tweet_id: int

class IncidentTweetRelationCreate(IncidentTweetRelationBase):
    pass

class IncidentTweetRelation(IncidentTweetRelationBase):
    created: datetime
    updated: datetime

    class Config:
        orm_mode = True


