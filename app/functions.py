from datetime import datetime, timedelta
from numpy.core.numeric import NaN
import pandas as pd
import io
from typing import List
import crud
import time
import ast
import requests as req
import os
import tweepy as tw
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import asyncio
import logging
from fastapi.encoders import jsonable_encoder

import requests
import sys

import ws_manager, preprocessing, crud

from fastapi.responses import StreamingResponse

coordinates = {}
coordinates_max_size = 1000 # Max number of locations saved
coordinates_max_age = 60 * 60 * 24 * 7 # Value in seconds

list_of_places = []
list_of_places_coordinates = []

log = logging.getLogger("smid")

def load_list_of_places():
    global list_of_places
    global list_of_places_coordinates
    df = pd.read_csv('datasets/NL.txt', sep='\t', header=None)
    df[1] = df[1].str.lower()
    list_of_places = df[1].to_list()
    list_of_places_coordinates = dict(zip(df[1], zip(df[4], df[5])))  

def create_csv_response(data, name, columns = None):
    if(columns != None):
        df = pd.DataFrame(data, columns=columns)
    else:
        df = pd.DataFrame(data)
        df = df.iloc[: , 1:]
    stream = io.StringIO()
    df.to_csv(stream, index = False)
    response = StreamingResponse(iter([stream.getvalue()]),
        media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=" + name
    return response

async def extract_media(ids: List[int], db):
    for status_id in ids:

        tweet = crud.get_tweet(db, status_id=status_id)
        if(tweet != None):
            if(tweet.__dict__["extended_entities"] != "None"):
                entities = ast.literal_eval(tweet.__dict__["extended_entities"])
            else:
                entities = ast.literal_eval(tweet.__dict__["entities"])
            if('media' in entities):
                media = entities["media"]
                if not os.path.exists("data/media/" + str(status_id)):
                    os.makedirs("data/media/" + str(status_id))
                    for i in range(0,len(media)):
                        r = req.get(media[i]["media_url_https"])
                        filename = media[i]["media_url_https"].split('/')[-1]
                        with open("data/media/" + str(status_id) + "/" + filename, "wb") as f:
                            f.write(r.content)
                    log.info("SMID - Media extracted for Tweet " + str(status_id))
                else:
                    log.info("SMID - Media for Tweet " + str(status_id) + " already existed")

def coordinates_for_place(place: str):
    global coordinates, coordinates_max_size, coordinates_max_age
    if(place in coordinates):
        if((float(coordinates[place]["timestamp"]) + coordinates_max_age) < time.time()):
            # Item in cache expired, renew
            coordinates[place] = {"coordinates": get_coordinates(place), "timestamp": time.time() }

        return coordinates[place]["coordinates"]
    else:
        if(len(coordinates) == coordinates_max_size):
            # Cache size exceeded, remove first item
            coordinates.pop(next(iter(coordinates)))
        coordinates[place] = {"coordinates": get_coordinates(place), "timestamp": time.time() }
        return coordinates[place]["coordinates"]

def get_coordinates(place):
    # Query OSM for coordinates
    # https://github.com/osm-search/Nominatim
    url = "https://nominatim.openstreetmap.org/search"
    params = dict(
        q=place,
        format="jsonv2"
    )
    r = req.get(url=url, params=params)
    data = [float(r.json()[0]["lat"]),float(r.json()[0]["lon"])]
    return data

# Code by Joren Wouters, adjusted for use within this API
def read_tweet(status):
    # If the tweet is truncated, use the full_text variable
    if status.truncated == True:
        text = status.extended_tweet['full_text']
    else:
        if(hasattr(status, 'text')):
            text = status.text
        else:
            text = status.full_text
    
    # If the tweet has extended entities (e.g. multiple images), set the attribute
    if (hasattr(status, 'extended_entities') and str(status.extended_entities) != 'nan'):
        extended_entities = status.extended_entities
    else:
        extended_entities = None
    
    # If the tweet is a retweet, set multiple variables
    if (hasattr(status, 'retweeted_status') and str(status.retweeted_status) != 'nan'):
        retweeted = True
        retweeted_status = status.retweeted_status
        retweeted_status_id = status.retweeted_status.id
    else:
        retweeted = False
        retweeted_status = None
        retweeted_status_id = None
        # retweeted_status_user_id = None
        
    # Creating this formatting so when exported to csv the tweet stays on one line
    text = "'" + text.replace('\n', ' ') + "'"
            
    status_dict = {
        "created_at": status.created_at,
        "status_id" : status.id,
        "text" : text,
        "coordinates": status.coordinates,
        "source" : status.source,
        "in_reply_to_status_id" : status.in_reply_to_status_id,
        "in_reply_to_user_id" : status.in_reply_to_user_id,
        "in_reply_to_screen_name" : status.in_reply_to_screen_name,
        "retweeted" : retweeted,
        "retweeted_status": retweeted_status,
        "retweeted_status_id" : retweeted_status_id,
        # "retweeted_status_user_id" : retweeted_status_user_id,
        "favorited" : status.favorited,
        "favorite_count" : status.favorite_count,
        "place" : status.place,
        "entities" : status.entities,
        "extended_entities" : extended_entities,
        "user": status.user,
        "user_id": status.user.id,
        "user_screen_name": status.user.screen_name,
    #     "possibly_sensitive" : status.possibly_sensitive,
        "lang" : status.lang
    }
    tweet = preprocessing.cleanTweet(status_dict)
    return tweet

class TweetListener(StreamListener):
    def __init__(self, keywords, db):
        super().__init__()
        self.keywords = keywords
        self.db = db
    
    def on_status(self, status):

        tweet = read_tweet(status)
        asyncio.run(ws_manager.get_manager().broadcast_tweet(tweet))
        try:
            crud.create_tweet(self.db, tweet=tweet)
        except:
            pass
        return True

    def on_exception(self, exception):
        log.error(exception)
        log.warn("SMID - Exception occurred in Twitter Stream Listener, it should automatically reconnect. Some Tweets may have been missed.")
        return
# End code by Joren Wouters

async def get_tweets_stream(keywords, db):
    log.info("SMID - Starting Twitter Stream Listener")
    # Set Twitter API Keys
    consumer_key= os.getenv("TW_CONSUMER_KEY")
    consumer_secret= os.getenv("TW_CONSUMER_SECRET")
    access_token= os.getenv("TW_ACCESS_TOKEN")
    access_token_secret= os.getenv("TW_ACCESS_TOKEN_SECRET")

    # Connect with Twitter API using Tweepy
    auth = tw.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    l = TweetListener(keywords=keywords, db=db)
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    stream.filter(track=keywords, is_async=True, stall_warnings=True)

def get_twitter_api():
    """
    Get tweepy api to communicate with the twitter server.

    Returns:
        (tweepy.api):           tweepy wrapper on twitter api. 
    """
    consumer_key = os.getenv("TW_CONSUMER_KEY")
    consumer_secret = os.getenv("TW_CONSUMER_SECRET")

    auth = tw.AppAuthHandler(consumer_key, consumer_secret)
    return tw.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

# Code by Laurens Müter, adjusted for use within this API
async def get_original_tweets_for_dataset(set, db):
    log.info("SMID - Retrieving Tweets from Twitter for dataset '" + set + "'")
    df = pd.read_csv("datasets/" + set + ".csv")

    e_list = []
    api = get_twitter_api()
    tweet_counter = 0
    error_count = 0

    for index, row in df.iterrows():
        try:
            status = api.get_status(row["id"], tweet_mode = "extended")
            tweet = read_tweet(status)
            await ws_manager.get_manager().broadcast_tweet(tweet)
            crud.create_tweet(db, tweet=tweet)
        except tw.TweepError as e:
            print("Tweet " + str(index) + " resulted in ERROR: " + str(e))
            e_list.append(index)
            error_count += 1
        except requests.exceptions.MissingSchema as e:
            print("Tweet " + str(index) + " resulted in ERROR: " + str(e))
            e_list.append(index)
            error_count += 1
        except requests.exceptions.InvalidURL as e:
            print("Tweet " + str(index) + " resulted in ERROR: " + str(e))
            e_list.append(index)
            error_count += 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            e_list.append(index)
            error_count += 1
            
        tweet_counter += 1
        if tweet_counter % 100 == 0:
            print("\n===\n{0} tweets processed\n===\n".format(tweet_counter))
# End code by Laurens Müter

async def replay_tweets(set, modifier, db):
    log.info("SMID - Replaying Tweets for dataset '" + set + "'")
    df = pd.read_csv("datasets/" + set + ".csv")
    df = df.sort_values(by=['created_at'], ascending=True)
    tweets = crud.get_tweets(db, df['id'].to_list())
    count = 0
    tweet = tweets[0].__dict__
    start = datetime.strptime(tweet['created_at'], '%Y-%m-%d %H:%M:%S') - timedelta(seconds=10)
    customTime = start

    while(count < len(tweets)):
        iterStart = int(time.time())

        if(datetime.strptime(tweet['created_at'], '%Y-%m-%d %H:%M:%S') < customTime):
            print(tweet['created_at'])
            
            await ws_manager.get_manager().broadcast_replay_tweet(jsonable_encoder(tweet))
            count += 1
            if(count < len(tweets)):
                tweet = tweets[count].__dict__

        await asyncio.sleep(1)

        delta = int(time.time()) - iterStart
        customTime = customTime + timedelta(seconds=delta * modifier)

async def replay_tweets_instant(set, db):
    log.info("SMID - Replaying Tweets for dataset '" + set + "'")
    df = pd.read_csv("datasets/" + set + ".csv")
    df = df.sort_values(by=['created_at'], ascending=True)
    tweets = crud.get_tweets(db, df['id'].to_list())
    count = 0
    tweet = tweets[0].__dict__
    while(count < len(tweets)):
        await ws_manager.get_manager().broadcast_replay_tweet(jsonable_encoder(tweet))
        count += 1
        if(count < len(tweets)):
            tweet = tweets[count].__dict__